﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Contact_app.Models;

namespace Contact_app.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactDirectoryController : ControllerBase
    {
        private readonly ContactDbContext _context;

        public ContactDirectoryController(ContactDbContext context)
        {
            _context = context;
        }

        // GET: api/ContactDirectory
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Directory>>> GetDirectories()
        {
            return await _context.Directories.ToListAsync();
        }

        //// GET: api/ContactDirectory/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Directory>> GetDirectory(int id)
        {
            var directory = await _context.Directories.FindAsync(id);

            if (directory == null)
            {
                return NotFound();
            }

            return directory;
        }

        // PUT: api/ContactDirectory/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDirectory(int id, Directory directory)
        {
            directory.ID = id;

            _context.Entry(directory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DirectoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ContactDirectory
        [HttpPost]
        public async Task<ActionResult<Directory>> PostDirectory(Directory directory)
        {
            _context.Directories.Add(directory);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDirectory", new { id = directory.ID }, directory);
        }

        // DELETE: api/ContactDirectory/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Directory>> DeleteDirectory(int id)
        {
            var directory = await _context.Directories.FindAsync(id);
            if (directory == null)
            {
                return NotFound();
            }

            _context.Directories.Remove(directory);
            await _context.SaveChangesAsync();

            return directory;
        }

        private bool DirectoryExists(int id)
        {
            return _context.Directories.Any(e => e.ID == id);
        }
    }
}
