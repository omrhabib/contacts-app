﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Contact_app.Migrations
{
    public partial class EmailDateCreatedandDateModifedaddded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Date_Created",
                table: "Directories",
                type: "nvarchar(100)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Date_Modified",
                table: "Directories",
                type: "nvarchar(100)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Directories",
                type: "nvarchar(100)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date_Created",
                table: "Directories");

            migrationBuilder.DropColumn(
                name: "Date_Modified",
                table: "Directories");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Directories");
        }
    }
}
